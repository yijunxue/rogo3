<?php

// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

namespace testing\behat\steps\frontend;

use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Tester\Exception\PendingException;
use testing\behat\selectors;
use Exception;

/**
 * User manipulation step definitions.
 *
 * @copyright Copyright (c) 2021 The University of Nottingham
 * @author Dr Joseph Baxter <joseph.baxter@nottingham.ac.uk>
 * @package testing
 * @subpackage behat
 */
trait User
{
    /**
     * Add a paper note to a student on the user profile
     *
     * @Given I add a note :note to the paper :paper
     * @param string $note the note
     * @param string $paper the paper
     * @throws Exception
     */
    public function iAddANoteToThePaper(string $note, string $paper): void
    {
        $element = $this->find('id_or_name', 'createname');
        $element->click();
        $this->i_focus_popup('Note');
        $this->fillDropDown('paperID', array($paper));
        $this->fillField('note', $note);
        $element = $this->find('button', 'Save');
        $element->click();
        $this->only_main_window();
        $this->i_focus_main_window();
    }
}
