requirejs(['tinyMCE', 'rogoconfig', 'jquery'], function (Tinymce, config, $) {
    Tinymce.init({
        selector: ".editorStandard",
        plugins: "visualchars nonbreaking paste lists code table image preview help",
        external_plugins: {
            'ruby-annotation': config.cfgrootpath +  "/plugins/texteditor/plugin_tinymce_texteditor/js/plugins/ruby-annotation/plugin.min.js",
            'maths-equation-editor': config.cfgrootpath +  "/plugins/texteditor/plugin_tinymce_texteditor/js/plugins/maths-equation-editor/plugin.min.js",
        },
        language: config.lang,
        language_url: '/plugins/texteditor/plugin_tinymce_texteditor/js/langs/' + config.lang + '.js',
        icons_url: config.cfgrootpath +  "/plugins/texteditor/plugin_tinymce_texteditor/icons/icons.js",
        icons: 'rogo',
        a11y_advanced_options: true,
        image_advtab: true,
        image_dimensions: false,
        image_uploadtab: true,
        images_file_types: 'gif,jpg,jpeg,png',
        images_upload_url: config.cfgrootpath +  "/plugins/texteditor/plugin_tinymce_texteditor/upload.php",
        menubar: false,
        statusbar: false,
        toolbar: "cut copy paste | undo | bold italic underline | subscript superscript | maths-equation-editor | ruby-annotation | alignleft aligncenter alignright | numlist bullist | image | table | code | preview | help",
        help_tabs: ['shortcuts', 'keyboardnav'],
    });

    Tinymce.init({
        selector: ".editorSimple",
        plugins: "visualchars nonbreaking paste code preview help",
        external_plugins: {
            'maths-equation-editor': config.cfgrootpath +  "/plugins/texteditor/plugin_tinymce_texteditor/js/plugins/maths-equation-editor/plugin.min.js",
        },
        language: config.lang,
        language_url: '/plugins/texteditor/plugin_tinymce_texteditor/js/langs/' + config.lang + '.js',
        icons_url: config.cfgrootpath +  "/plugins/texteditor/plugin_tinymce_texteditor/icons/icons.js",
        icons: 'rogo',
        a11y_advanced_options: true,
        image_advtab: true,
        image_dimensions: false,
        image_uploadtab: true,
        images_file_types: 'gif,jpg,jpeg,png',
        images_upload_url: config.cfgrootpath +  "/plugins/texteditor/plugin_tinymce_texteditor/upload.php",
        menubar: false,
        statusbar: false,
        toolbar: "cut copy paste | undo | bold italic underline | subscript superscript | maths-equation-editor | code | preview | help",
        help_tabs: ['shortcuts', 'keyboardnav'],
    });

    Tinymce.init({
        selector: ".editorBasic",
        plugins: "visualchars nonbreaking paste help",
        a11y_advanced_options: true,
        image_advtab: true,
        image_dimensions: false,
        image_uploadtab: true,
        images_file_types: 'gif,jpg,jpeg,png',
        images_upload_url: config.cfgrootpath +  "/plugins/texteditor/plugin_tinymce_texteditor/upload.php",
        menubar: false,
        statusbar: false,
        toolbar: "cut copy paste pastetext| undo | removeformat | help",
        help_tabs: ['shortcuts', 'keyboardnav'],
        language: config.lang,
        language_url: '/plugins/texteditor/plugin_tinymce_texteditor/js/langs/' + config.lang + '.js',
    });
});
