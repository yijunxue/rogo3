<?php

// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * Delete an Ebel template - Admin only.
 *
 * @author Simon Wilkinson
 * @version 1.0
 * @copyright Copyright (c) 2014 The University of Nottingham
 * @package
 */

require '../include/admin_auth.inc';
require '../include/errors.php';
  
$gridID = check_var('gridID', 'POST', true, false, true);

$row_no = 0;

$result = $mysqli->prepare('SELECT name FROM ebel_grid_templates WHERE id = ?');
$result->bind_param('i', $gridID);
$result->execute();
$result->store_result();
$result->bind_result($grid_name);
$result->fetch();
$row_no = $result->num_rows;
$result->close();

if ($row_no == 0) {
    $contactemail = support::get_email();
    $msg = sprintf($string['furtherassistance'], $contactemail, $contactemail);
    $notice->display_notice_and_exit($mysqli, $string['pagenotfound'], $msg, $string['pagenotfound'], '../artwork/page_not_found.png', '#C00000', true, true);
}

$result = $mysqli->prepare('DELETE FROM ebel_grid_templates WHERE id = ?');
$result->bind_param('i', $gridID);
$result->execute();
$result->close();

$render = new render($configObject);
$lang['title'] = $string['title'];
$lang['success'] = $string['msg'];
$data = array();
$render->render($data, $lang, 'admin/do_delete.html');

$mysqli->close();
