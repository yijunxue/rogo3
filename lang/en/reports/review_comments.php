<?php

// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['internalreport'] = 'Internal Examiner Comments Report';
$string['externalreport'] = 'External Examiner Comments Report';
$string['internalcomments'] = 'Internal Comments Q';
$string['externalcomments'] = 'External Comments Q';
$string['editquestion'] = 'Edit Question';
$string['reviewer'] = 'Reviewer';
$string['comment'] = 'Comment';
$string['action'] = 'Action';
$string['response'] = 'Response';
$string['nocomment'] = 'No comment';
$string['na'] = 'N/A';
$string['Read - disagree'] = 'Read - disagree';
$string['Read - actioned'] = 'Read - actioned';
$string['Not actioned'] = 'Not actioned';
$string['notreviewed'] = 'Not reviewed';
$string['noresponse'] = 'No response made';
$string['noreviewers'] = '<strong>Warning:</strong> There have been no reviews of this paper.';
$string['screen'] = 'Screen';
$string['papernotfound'] = 'Paper not Found';
$string['furtherassistance'] = 'For further assistance contact: <a href="mailto:%s">%s</a>';
$string['reviewers'] = 'Reviewers';
$string['started'] = 'Started';
$string['completed'] = 'Completed';
$string['generalpapercomments'] = 'General Paper Comments';
$string['cannotcomment'] = 'Cannot Comment';
$string['threeinfo'] = 'Hold the left mouse button to rotate the object, hold the right button to pan, and use the mouse wheel to zoom in/out.';
$string['threereset'] = 'Reset';
$string['threeload'] = 'Load';
$string['threeplyerror'] = 'Your browser does not support the display of %s files.';
$string['randomwarning'] = '<strong>Warning:</strong> Random question block questions cannot be analysed in this report.';
$string['keywordwarning'] = '<strong>Warning:</strong> Keyword-based questions cannot be analysed in this report.';
