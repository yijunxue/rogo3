<?php

// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['frequencydiscrimination'] = 'Frequency & Discrimination Analysis';
$string['reporttitle'] = 'Frequency & Discrimination (U-L) Analysis Report';
$string['PaperNotAttempted'] = 'This paper has not been attempted by anyone.';
$string['NotEnoughData'] = 'Not enough data to calculate upper and lower groups. Please select a higher percentage.';
$string['AllItemsCorrect'] = 'All items correct';
$string['totalcandidatenumber'] = 'Candidates';
$string['FullMarks'] = 'Full marks';
$string['PartialMarks'] = 'Partial marks';
$string['Incorrect'] = 'Incorrect';
$string['Correct'] = 'Correct';
$string['True'] = 'True';
$string['False'] = 'False';
$string['TopGroup'] = 'Top Group';
$string['BottomGroup'] = 'Bottom Group';
$string['meanWordCount'] = 'mean word count';
$string['groupsizes'] = 'Group sizes';
$string['pergroup'] = 'per group';
$string['boldstems'] = 'Bold';
$string['correctanswers'] = 'represent correct answer(s)';
$string['p_definition'] = 'Item Difficulty (proportion of students answering item correctly)';
$string['d_definition'] = 'discrimination value';
$string['t_definition'] = 'percentage of the <strong>total</strong> cohort answering item';
$string['u_definition'] = 'percent of <strong>upper</strong> group answering item';
$string['l_definition'] = 'percentage of <strong>lower</strong> group answering item';
$string['X_definition'] = 'Number of students that left this question unanswered';
$string['warning'] = 'Warning';
$string['p_warning'] = '<strong>p < 0.2</strong> (i.e. very hard)';
$string['d_warning'] = "<strong>d < 0.15</strong> (i.e. low)<br />Investigate the red flags and if you conclude that the item is poor, exclude using the <img src=\"../artwork/exclude_off.gif\" style=\"cursor:pointer; background-color:white\" width=\"23\" height=\"22\" alt=\"Exclude\" /> icon and then click 'Save' at the bottom";
$string['summary'] = 'Summary';
$string['msg'] = 'The number of items can be more than the number of questions as dichotomous, labelling and extended matching questions are made up of multiple items each with their own p and d values.';
$string['difficulty'] = 'Difficulty';
$string['discrimination'] = 'Discrimination';
$string['noofitems'] = 'No of items';
$string['veryeasy'] = 'Very Easy';
$string['easy'] = 'Easy';
$string['moderate'] = 'Moderate';
$string['hard'] = 'Hard';
$string['veryhard'] = 'Very Hard';
$string['mean'] = 'Mean';
$string['highest'] = 'Highest';
$string['high'] = 'High';
$string['intermediate'] = 'Intermediate';
$string['low'] = 'Low';
$string['save'] = 'Save Exclusions';
$string['screen'] = 'Screen';
$string['warning1'] = 'Warning: Difficulty is less than 0.2';
$string['warning2'] = 'Warning: Discrimination less than 0.15';
$string['abstain'] = 'Abstain';
$string['unmarkedscripts'] = '%d unmarked scripts';
$string['randomwarning'] = '<strong>Warning:</strong> Random question block questions cannot be analysed in this report.';
$string['keywordwarning'] = '<strong>Warning:</strong> Keyword-based questions cannot be analysed in this report.';
$string['paperpublishedwarning'] = '<strong>Paper grades have been published.</strong>&nbsp;&nbsp;&nbsp;Marking adjustment can no longer occur.';
$string['paperlockedclick'] = 'Click for more details';
$string['threeinfo'] = 'Hold the left mouse button to rotate the object, hold the right button to pan,  and use the mouse wheel to zoom in/out.';
$string['threereset'] = 'Reset';
$string['threeload'] = 'Load';
$string['threeplyerror'] = 'Your browser does not support the display of %s files.';
$string['unanswered'] = 'Unanswered';
$string['rank_1'] = 'st';
$string['rank_2'] = 'nd';
$string['rank_3'] = 'rd';
$string['rank_other'] = 'th';
$string['marks'] = 'marks';
$string['mark'] = 'mark';
$string['unmarked'] = 'unmarked';
