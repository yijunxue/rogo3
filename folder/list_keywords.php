<?php

// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @author Simon Wilkinson
 * @version 1.0
 * @copyright Copyright (c) 2014 The University of Nottingham
 * @package
 */

require '../include/staff_auth.inc';
require '../include/errors.php';
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta http-equiv="content-type" content="text/html;charset=<?php echo $configObject->get('cfg_page_charset') ?>" />
  <title><?php echo page::title('Rog&#333;: ' . $string['keywords']); ?></title>
  
  <link rel="stylesheet" type="text/css" href="../css/body.css" />
  <link rel="stylesheet" type="text/css" href="../css/header.css" />
  <link rel="stylesheet" type="text/css" href="../css/submenu.css" />
  <link rel="stylesheet" type="text/css" href="../css/keywordlist.css" />

  <script id="rogoconfig" data-lang="<?php echo \LangUtils::getLang($cfg_web_root); ?>" data-root="<?php echo $configObject->get('cfg_root_path'); ?>"></script>
  <script src='../js/require.js'></script>
  <script src='../js/main.min.js'></script>
  <script src="../js/keywordlistinit.min.js"></script>
</head>

<body>
<?php
  require '../include/toprightmenu.inc';
    
    echo draw_toprightmenu(237);

  $keyword_list = array();
  
if (isset($_GET['module']) and $_GET['module'] != '') {
    $module_code = module_utils::get_moduleid_from_id($_GET['module'], $mysqli);
    if (!$module_code) {
        $contactemail = support::get_email();
        $msg = sprintf($string['furtherassistance'], $contactemail, $contactemail);
        $notice->display_notice_and_exit($mysqli, $string['pagenotfound'], $msg, $string['pagenotfound'], '../artwork/page_not_found.png', '#C00000', true, true);
    }

    // Get team keywords
    $result = $mysqli->prepare("SELECT id, keyword FROM keywords_user WHERE keyword_type = 'team' AND userID = ? ORDER BY keyword");
    $result->bind_param('i', $_GET['module']);
    $result->execute();
    $result->bind_result($keywordID, $keyword);
    while ($result->fetch()) {
        $keyword_list[$keywordID] = $keyword;
    }
    $result->close();
} else {
    // Get personal keywords
    $result = $mysqli->prepare("SELECT id, keyword FROM keywords_user WHERE keyword_type = 'personal' AND userid = ? ORDER BY keyword");
    $result->bind_param('i', $userObject->get_user_ID());
    $result->execute();
    $result->bind_result($keywordID, $keyword);
    while ($result->fetch()) {
        $keyword_list[$keywordID] = $keyword;
    }
    $result->close();
}

  require '../include/folder_keyword_options.inc';
?>
<div id="content">

<div class="head_title">
  <div><img src="../artwork/toprightmenu.gif" id="toprightmenu_icon" /></div>
<?php
if (isset($_GET['module']) and $_GET['module'] != '') {
    $module_code = module_utils::get_moduleid_from_id($_GET['module'], $mysqli);
    echo '<div class="breadcrumb"><a href="../index.php">' . $string['home'] . '</a><img src="../artwork/breadcrumb_arrow.png" class="breadcrumb_arrow" alt="-" /><a href="../module/index.php?module=' . $_GET['module'] . '">' . $module_code . '</a></div>';
    echo '<div class="page_title" style="font-weight:normal">' . sprintf($string['modulekeywords'], $module_code) . "</div>\n";
} else {
    echo '<div class="breadcrumb"><a href="../index.php">' . $string['home'] . '</a></div>';
    echo '<div class="page_title" style="font-weight:normal">' . $string['mypersonalkeywords'] . "</div>\n";
}
?>
</div>
  
<table class="header">
<?php
foreach ($keyword_list as $keywordID => $keyword) {
    echo "<tr class=\"qline\" id=\"link_$keywordID\" data-keywordid=\"$keywordID\"><td colspan=\"2\">&nbsp;$keyword</td></tr>\n";
}
$mysqli->close();
?>
</table>
</div>
</body>
<?php
// Dataset.
$render = new render($configObject);
$miscdataset['name'] = 'dataset';
$miscdataset['attributes']['module'] = $module;
$render->render($miscdataset, array(), 'dataset.html');
?>
</body>
</html>
