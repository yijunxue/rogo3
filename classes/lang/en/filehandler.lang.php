<?php

// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['invalidpath'] = ' has an invalid path';
$string['nofilename'] = 'No filename supplied.';
$string['csvonly'] = 'File has an invalid file extension. Only .csv is supported.';
$string['maxfilesize'] = 'The uploaded file was bigger than the maximum allowed.';
$string['partialupload'] = 'File partialy uploaded.';
$string['nofileuploaded'] = 'No file was uploaded.';
$string['notempdir'] = 'No temp directory.';
$string['unknownissue'] = 'Unknown problem.';
$string['doesnotexist'] = ' does not exist';
$string['cannotberead'] = ' cannot be read';
$string['invalidheaders'] = ' has invalid headers';
$string['cannotwritebom'] = 'Cannot write UTF-8 byte order mark';
$string['cannotwriteheaders'] = 'Cannot write file header';
$string['noheaders'] = 'No file header to write';
$string['cannotwritefile'] = 'Cannot write to file';
$string['cannotwriteline'] = 'Cannot write line to file';
